<?php

namespace Torside\SlovakLocations\Exceptions;

final class GeoShapeException extends \Exception
{
}