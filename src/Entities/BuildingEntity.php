<?php

namespace Torside\SlovakLocations\Entities;

final class BuildingEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var int $propertyRegistrationNumber */
    protected $propertyRegistrationNumber;

    /** @var int $districtIdentifier */
    protected $districtIdentifier;

    /** @var int $municipalityIdentifier */
    protected $municipalityIdentifier;

    /** @var int $buildingTypeCode */
    protected $buildingTypeCode;

    /** @var bool $containsFlats */
    protected $containsFlats;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'propertyRegistrationNumber',
        'districtIdentifier',
        'municipalityIdentifier',
        'buildingTypeCode',
        'containsFlats'
    ];


    /**
     * BuildingEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->propertyRegistrationNumber = $data['propertyRegistrationNumber'];
        $this->districtIdentifier = $data['districtIdentifier'];
        $this->municipalityIdentifier = $data['municipalityIdentifier'];
        $this->buildingTypeCode = $data['buildingTypeCode'];
        $this->containsFlats = $data['containsFlats'];
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return int
     */
    public function getPropertyRegistrationNumber(): int
    {
        return $this->propertyRegistrationNumber;
    }

    /**
     * @return int
     */
    public function getDistrictIdentifier(): int
    {
        return $this->districtIdentifier;
    }

    /**
     * @return int
     */
    public function getMunicipalityIdentifier(): int
    {
        return $this->municipalityIdentifier;
    }

    /**
     * @return int
     */
    public function getBuildingTypeCode(): int
    {
        return $this->buildingTypeCode;
    }

    /**
     * @return bool
     */
    public function getContainsFlats(): bool
    {
        return $this->containsFlats;
    }

}