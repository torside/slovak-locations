<?php

namespace Torside\SlovakLocations\Entities;

final class EntranceEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var int $propertyRegistrationNumberIdentifier */
    protected $propertyRegistrationNumberIdentifier;

    /** @var string $postalCode */
    protected $postalCode;

    /** @var int $streetNameIdentifier */
    protected $streetNameIdentifier;

    /** @var int $buildingNumber */
    protected $buildingNumber;

    /** @var GeoPointEntity $coordinates */
    protected $coordinates;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'propertyRegistrationNumberIdentifier',
        'postalCode',
        'streetNameIdentifier',
        'buildingNumber',
        'coordinates'
    ];

    /**
     * EntranceEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->propertyRegistrationNumberIdentifier = $data['propertyRegistrationNumberIdentifier'];
        $this->postalCode = $data['postalCode'];
        $this->streetNameIdentifier = $data['streetNameIdentifier'];
        $this->buildingNumber = $data['buildingNumber'];
        $this->coordinates = new GeoPointEntity($data['coordinates']['lat'], $data['coordinates']['lon']);
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return int
     */
    public function getPropertyRegistrationNumberIdentifier(): int
    {
        return $this->propertyRegistrationNumberIdentifier;
    }

    /**
     * @return string
     */
    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    /**
     * @return int
     */
    public function getStreetNameIdentifier(): int
    {
        return $this->streetNameIdentifier;
    }

    /**
     * @return int
     */
    public function getBuildingNumber(): int
    {
        return $this->buildingNumber;
    }

    /**
     * @return GeoPointEntity
     */
    public function getCoordinates(): GeoPointEntity
    {
        return $this->coordinates;
    }

}