<?php

namespace Torside\SlovakLocations\Entities;

final class DistrictEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var string $districtName */
    protected $districtName;

    /** @var string $districtCode */
    protected $districtCode;

    /** @var int $municipalityIdentifier */
    protected $municipalityIdentifier;

    /** @var GeoShapeEntity $boundaries */
    protected $boundaries;

    /** @var GeoShapeEntity $viewport */
    protected $viewport;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'districtName',
        'districtCode',
        'municipalityIdentifier',
        'viewport',
        'boundaries'
    ];

    /**
     * DistrictEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->districtName = $data['districtName'];
        $this->districtCode = $data['districtCode'];
        $this->municipalityIdentifier = $data['municipalityIdentifier'];

        $this->boundaries = !empty($data['boundaries']) ? new GeoShapeEntity($data['boundaries']['type'], $data['boundaries']['coordinates']) : null;
        $this->viewport = !empty($data['viewport']) ? new GeoShapeEntity($data['viewport']['type'], $data['viewport']['coordinates']) : null;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function getDistrictName(): string
    {
        return $this->districtName;
    }

    /**
     * @return int
     */
    public function getDistrictCode(): int
    {
        return $this->districtCode;
    }

    /**
     * @return int
     */
    public function getMunicipalityIdentifier(): int
    {
        return $this->municipalityIdentifier;
    }

    /**
     * @return GeoShapeEntity|null
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

    /**
     * @return GeoShapeEntity|null
     */
    public function getViewport()
    {
        return $this->viewport;
    }

}