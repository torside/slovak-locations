<?php

namespace Torside\SlovakLocations\Entities;

use Torside\SlovakLocations\Exceptions\GeoShapeException;

final class GeoShapeEntity
{

    const GEO_SHAPE_TYPE_POINT = 'Point';
    const GEO_SHAPE_TYPE_POLYGON = 'Polygon';
    const GEO_SHAPE_TYPE_MULTIPOLYGON = 'MultiPolygon';
    const GEO_SHAPE_TYPE_ENVELOPE = 'envelope';

    const ALLOWED_GEO_SHAPE_TYPES = [
        self::GEO_SHAPE_TYPE_POINT,
        self::GEO_SHAPE_TYPE_POLYGON,
        self::GEO_SHAPE_TYPE_MULTIPOLYGON,
        self::GEO_SHAPE_TYPE_ENVELOPE
    ];

    /** @var string $type */
    protected $type;

    /** @var array $coordinates */
    protected $coordinates;

    /** @var array $properties */
    protected $properties = [
        'type',
        'coordinates'
    ];

    /**
     * GeoShapeCollection constructor.
     *
     * @param string|null $type
     * @param array $coordinates
     *
     * @internal param array $items
     */
    public function __construct(string $type = null, array $coordinates = [])
    {
        $this->setType($type)
            ->setCoordinates($coordinates);
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'coordinates' => $this->coordinates
        ];
    }

    /**
     * @param string $type
     *
     * @return GeoShapeEntity
     *
     * @throws GeoShapeException
     */
    private function setType(string $type): GeoShapeEntity
    {
        if (empty($type)) {
            throw new GeoShapeException('GeoShape type must be specififed.', 0);
        }

        if (!in_array($type, self::ALLOWED_GEO_SHAPE_TYPES)) {
            throw new GeoShapeException("Invalid GeoShape type '{$type}'. Allowed types are: " . implode(', ', self::ALLOWED_GEO_SHAPE_TYPES), 1);
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param $coordinates
     *
     * @return GeoShapeEntity
     *
     * @throws GeoShapeException
     */
    private function setCoordinates($coordinates): GeoShapeEntity
    {
        if (empty($coordinates)) {
            throw new GeoShapeException('GeoShape coordinates must be specififed.', 3);
        }

        if (!is_array($coordinates)) {
            throw new GeoShapeException('GeoShape coordinates must be an array.', 4);
        }

        $this->validateCoordinates($coordinates);

        $this->coordinates = $coordinates;

        return $this;
    }

    /**
     * @return array
     */
    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    /**
     * @param array $coordinates
     *
     * @throws GeoShapeException
     */
    private function validateCoordinates(array $coordinates)
    {
        switch ($this->type) {
            case self::GEO_SHAPE_TYPE_POINT:
                if ($this->isPoint($coordinates) === false) {
                    throw new GeoShapeException('Invalid point data.', 5);
                }
                break;
            case self::GEO_SHAPE_TYPE_POLYGON:
                if ($this->isPolygon($coordinates) === false) {
                    throw new GeoShapeException('Invalid polygon data.', 6);
                }
                break;
            case self::GEO_SHAPE_TYPE_MULTIPOLYGON:
                if ($this->isMultiPolygon($coordinates) === false) {
                    throw new GeoShapeException('Invalid multipolygon data.', 7);
                }
                break;
            case self::GEO_SHAPE_TYPE_ENVELOPE:
                if ($this->isEnvelope($coordinates) === false) {
                    //throw new GeoShapeException('Invalid envelope data.', 8);
                    exit;
                }
                break;
        }
    }

    /**
     * @param array $arr
     *
     * @return bool
     */
    private function isSequential(array $arr): bool
    {
        return array_keys($arr) === range(0, count($arr) - 1);
    }

    /**
     * @param array $coordinates
     *
     * @return bool
     */
    private function isPoint(array $coordinates): bool
    {
        if ($this->isSequential($coordinates) === false) {
            return false;
        }

        $coordinates = array_filter($coordinates, function ($n) {
            return is_float($n) || is_int($n);
        });

        if (count($coordinates) != 2) {
            return false;
        }

        $longitude = $coordinates[0];
        $latitude = $coordinates[1];

        return $longitude <= 180 && $longitude >= -180 && $latitude <= 90 && $latitude >= -90;
    }

    /**
     * @param array $coordinates
     *
     * @return bool
     */
    private function isPolygon(array $coordinates): bool
    {
        if ($this->isSequential($coordinates) === false) {
            return false;
        }

        foreach ($coordinates as $singlePolygon) {
            if ($this->isSequential($singlePolygon) === false || count($singlePolygon) < 4) {
                return false;
            }

            foreach ($singlePolygon as $point) {
                if ($this->isPoint($point) === false) {
                    return false;
                }
            }

            if ($singlePolygon[0] !== $singlePolygon[count($singlePolygon) - 1]) {
                return false;
            }

        }

        return true;
    }

    /**
     * @param array $coordinates
     *
     * @return bool
     */
    private function isMultiPolygon(array $coordinates): bool
    {
        if ($this->isSequential($coordinates) === false) {
            return false;
        }

        foreach ($coordinates as $polygon) {
            if ($this->isPolygon($polygon) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $coordinates
     *
     * @return bool
     */
    private function isEnvelope(array $coordinates): bool
    {
        if ($this->isSequential($coordinates) === false || count($coordinates) !== 2) {
            return false;
        }

        foreach ($coordinates as $point) {
            if ($this->isPoint($point) === false) {
                return false;
            }
        }

        return true;
    }

}