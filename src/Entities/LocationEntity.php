<?php

namespace Torside\SlovakLocations\Entities;

use Illuminate\Contracts\Support\Arrayable;

class LocationEntity implements Arrayable
{

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId'
    ];

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        /** @var array $array */
        $array = [];
        foreach ($this->properties as $property) {
            if (property_exists($this, $property)) {
                if (gettype($this->$property) === 'object' && is_callable([$this->$property, 'toArray'])) {
                    $array[$property] = $this->$property->toArray();
                    continue;
                }
                $array[$property] = $this->$property;
            }
        }

        return $array;
    }
}