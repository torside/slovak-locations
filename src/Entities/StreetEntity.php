<?php

namespace Torside\SlovakLocations\Entities;

final class StreetEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var string $streetName */
    protected $streetName;

    /** @var int $districtIdentifiers */
    protected $districtIdentifiers;

    /** @var int $municipalityIdentifiers */
    protected $municipalityIdentifiers;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'streetName',
        'districtIdentifiers',
        'municipalityIdentifiers'
    ];

    /**
     * StreetEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->streetName = $data['streetName'];
        $this->districtIdentifiers = $data['districtIdentifiers'];
        $this->municipalityIdentifiers = $data['municipalityIdentifiers'];
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function getStreetName(): string
    {
        return $this->streetName;
    }

    /**
     * @return int
     */
    public function getDistrictIdentifiers(): int
    {
        return $this->districtIdentifiers;
    }

    /**
     * @return int
     */
    public function getMunicipalityIdentifiers(): int
    {
        return $this->municipalityIdentifiers;
    }

}