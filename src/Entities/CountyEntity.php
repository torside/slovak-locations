<?php

namespace Torside\SlovakLocations\Entities;

final class CountyEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var string $regionName */
    protected $countyName;

    /** @var string $regionCode */
    protected $countyCode;

    /** @var int $regionIdentifier */
    protected $regionIdentifier;

    /** @var GeoShapeEntity $boundaries */
    protected $boundaries;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'countyName',
        'countyCode',
        'regionIdentifier',
        'boundaries'
    ];

    /**
     * CountyEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->countyName = $data['countyName'];
        $this->countyCode = $data['countyCode'];
        $this->regionIdentifier = $data['regionIdentifier'];

        $this->boundaries = !empty($data['boundaries']) ? new GeoShapeEntity($data['boundaries']['type'], $data['boundaries']['coordinates']) : null;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function getCountyName(): string
    {
        return $this->countyName;
    }

    /**
     * @return string
     */
    public function getCountyCode(): string
    {
        return $this->countyCode;
    }

    /**
     * @return int
     */
    public function getRegionIdentifier(): int
    {
        return $this->regionIdentifier;
    }

    /**
     * @return GeoShapeEntity|null
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

}