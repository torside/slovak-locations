<?php

namespace Torside\SlovakLocations\Entities;

final class RegionEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var string $regionName */
    protected $regionName;

    /** @var string $regionCode */
    protected $regionCode;

    /** @var GeoShapeEntity $boundaries */
    protected $boundaries;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'regionName',
        'regionCode',
        'boundaries'
    ];

    /**
     * RegionEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->regionName = $data['regionName'];
        $this->regionCode = $data['regionCode'];
        $this->boundaries = !empty($data['boundaries']) ? new GeoShapeEntity($data['boundaries']['type'], $data['boundaries']['coordinates']) : null;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function getRegionName(): string
    {
        return $this->regionName;
    }

    /**
     * @return string
     */
    public function getRegionCode(): string
    {
        return $this->regionCode;
    }

    /**
     * @return GeoShapeEntity|null
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

}