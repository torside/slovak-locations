<?php

namespace Torside\SlovakLocations\Entities;

final class MunicipalityEntity extends LocationEntity
{

    /** @var int $objectId */
    protected $objectId;

    /** @var int $versionId */
    protected $versionId;

    /** @var string $municipalityName */
    protected $municipalityName;

    /** @var string $municipalityCode */
    protected $municipalityCode;

    /** @var int $countyIdentifier */
    protected $countyIdentifier;

    /** @var string $status */
    protected $status;

    /** @var int $nominatimRef */
    protected $nominatimRef;

    /** @var GeoShapeEntity $boundaries */
    protected $boundaries;

    /** @var array $properties */
    protected $properties = [
        'objectId',
        'versionId',
        'municipalityName',
        'municipalityCode',
        'countyIdentifier',
        'status',
        'nominatimRef',
        'boundaries'
    ];

    /**
     * MunicipalityEntity constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->objectId = $data['objectId'];
        $this->versionId = $data['versionId'];
        $this->municipalityName = $data['municipalityName'];
        $this->municipalityCode = $data['municipalityCode'];
        $this->countyIdentifier = $data['countyIdentifier'];
        $this->status = $data['status'];
        $this->nominatimRef = $data['nominatimRef'];

        $this->boundaries = !empty($data['boundaries']) ? new GeoShapeEntity($data['boundaries']['type'], $data['boundaries']['coordinates']) : null;
    }

    /**
     * @return int
     */
    public function getObjectId(): int
    {
        return $this->objectId;
    }

    /**
     * @return int
     */
    public function getVersionId(): int
    {
        return $this->versionId;
    }

    /**
     * @return string
     */
    public function getMunicipalityName(): string
    {
        return $this->municipalityName;
    }

    /**
     * @return string
     */
    public function getMunicipalityCode(): string
    {
        return $this->municipalityCode;
    }

    /**
     * @return int
     */
    public function getCountyIdentifier(): int
    {
        return $this->countyIdentifier;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getNominatimRef(): string
    {
        return $this->nominatimRef;
    }

    /**
     * @return GeoShapeEntity|null
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

}