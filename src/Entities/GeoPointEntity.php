<?php

namespace Torside\SlovakLocations\Entities;

final class GeoPointEntity extends LocationEntity
{

    /** @var float $lat */
    protected $lat;

    /** @var float $lon */
    protected $lon;

    /** @var array $properties */
    protected $properties = [
        'lat',
        'lon'
    ];

    /**
     * GeoPointEntity constructor.
     *
     * @param float $lat
     * @param float $lon
     */
    public function __construct(float $lat, float $lon)
    {
        $this->lat = $lat;
        $this->lon = $lon;
    }

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return float
     */
    public function getLon(): float
    {
        return $this->lon;
    }

}