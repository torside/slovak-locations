<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\DistrictCollection;
use Torside\SlovakLocations\Entities\DistrictEntity;

final class DistrictProvider
{

    const INDEX_NAME = 'districts';

    /** @var string $entity */
    private $entity = DistrictEntity::class;

    /** @var string $collection */
    private $collection = DistrictCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * DistrictProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for DistrictProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return DistrictEntity
     */
    public function getDistrictById(int $objectId): DistrictEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new DistrictEntity($data['_source']);
    }

    /**
     * Returns districts by municipality.
     *
     * @param int $parentObjectId
     *
     * @return DistrictCollection
     */
    public function getDistrictsByMunicipality(int $parentObjectId): DistrictCollection
    {
        /** @var array $properties */
        $properties = ['objectId', 'versionId', 'districtName', 'districtCode', 'municipalityIdentifier', 'viewport'];

        /** @var string $parentField */
        $parentField = 'municipalityIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => array_merge(
                $this->locationProvider->createSimpleFilter($parentField, $parentObjectId),
                ["_source" => $properties]
            )
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns districts by municipality count.
     *
     * @param int $parentObjectId
     *
     * @return int
     */
    public function getDistrictsByMunicipalityCount(int $parentObjectId): int
    {
        return $this->getDistrictsByMunicipality($parentObjectId)
            ->count();
    }

}