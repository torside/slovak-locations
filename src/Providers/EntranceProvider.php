<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\EntranceCollection;
use Torside\SlovakLocations\Entities\EntranceEntity;
use Torside\SlovakLocations\Entities\GeoPointEntity;

final class EntranceProvider
{

    const INDEX_NAME = 'entrances';

    /** @var string $entity */
    private $entity = EntranceEntity::class;

    /** @var string $collection */
    private $collection = EntranceCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * EntranceProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for EntranceProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return EntranceEntity
     */
    public function getEntranceById(int $objectId): EntranceEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new EntranceEntity($data['_source']);
    }

    /**
     * Returns entrances by street.
     *
     * @param $parentObjectId
     *
     * @return EntranceCollection
     */
    public function getEntrancesByStreet($parentObjectId): EntranceCollection
    {
        /** @var string $parentField */
        $parentField = 'streetNameIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns entrances by street.
     *
     * @param $parentObjectId
     *
     * @return EntranceCollection
     */
    public function getEntrancesByBuilding($parentObjectId): EntranceCollection
    {
        /** @var string $parentField */
        $parentField = 'propertyRegistrationNumberIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Finds nearest entrances by given props.
     *
     * @param GeoPointEntity $coordinates
     * @param int $size
     * @param string $distance
     *
     * @return EntranceCollection
     */
    public function getEntrancesByCoordinates(GeoPointEntity $coordinates, int $size = 1, string $distance = '40km'): EntranceCollection
    {
        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createGeoDistanceFilter($coordinates, $size, $distance, $geoPointField = 'coordinates')
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

}