<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\RegionCollection;
use Torside\SlovakLocations\Entities\RegionEntity;

final class RegionProvider
{

    const INDEX_NAME = 'regions';

    /** @var string $entity */
    private $entity = RegionEntity::class;

    /** @var string $collection */
    private $collection = RegionCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * RegionProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for RegionProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return RegionEntity
     */
    public function getRegionById(int $objectId): RegionEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new RegionEntity($data['_source']);
    }

    /**
     * Returns all regions.
     *
     * @return RegionCollection
     */
    public function getRegions(): RegionCollection
    {
        /** @var array $properties */
        $properties = ['objectId', 'versionId', 'regionName', 'regionCode'];

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => [
                "_source" => $properties
            ]
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

}