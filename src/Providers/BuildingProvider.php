<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\BuildingCollection;
use Torside\SlovakLocations\Entities\BuildingEntity;

final class BuildingProvider
{

    const INDEX_NAME = 'buildings';

    /** @var string $entity */
    private $entity = BuildingEntity::class;

    /** @var string $collection */
    private $collection = BuildingCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * BuildingProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for BuildingProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return BuildingEntity
     */
    public function getBuildingById(int $objectId): BuildingEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new BuildingEntity($data['_source']);
    }

    /**
     * Returns buildings by municipality.
     *
     * @param $parentObjectId
     *
     * @return BuildingCollection
     */
    public function getBuildingsByMunicipality($parentObjectId): BuildingCollection
    {
        /** @var string $parentField */
        $parentField = 'municipalityIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns buildings by district.
     *
     * @param $parentObjectId
     *
     * @return BuildingCollection
     */
    public function getBuildingsByDistrict($parentObjectId): BuildingCollection
    {
        /** @var string $parentField */
        $parentField = 'districtIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns buildings by municipality count.
     *
     * @param $parentObjectId
     *
     * @return int
     */
    public function getBuildingsByMunicipalityCount($parentObjectId): int
    {
        return $this->getBuildingsByMunicipality($parentObjectId)
            ->count();
    }

}