<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\StreetCollection;
use Torside\SlovakLocations\Entities\StreetEntity;

final class StreetProvider
{

    const INDEX_NAME = 'streets';

    /** @var string $entity */
    private $entity = StreetEntity::class;

    /** @var string $collection */
    private $collection = StreetCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * StreetProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for StreetProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return StreetEntity
     */
    public function getStreetById(int $objectId): StreetEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new StreetEntity($data['_source']);
    }

    /**
     * Returns streets by municipality.
     *
     * @param int $parentObjectId
     *
     * @return StreetCollection
     */
    public function getStreetsByMunicipality(int $parentObjectId): StreetCollection
    {
        /** @var string $parentField */
        $parentField = 'municipalityIdentifiers';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns streets by district.
     *
     * @param int $parentObjectId
     *
     * @return StreetCollection
     */
    public function getStreetsByDistrict(int $parentObjectId): StreetCollection
    {
        /** @var string $parentField */
        $parentField = 'districtIdentifiers';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => $this->locationProvider->createSimpleFilter($parentField, $parentObjectId)
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns streets by municipality count.
     *
     * @param int $parentObjectId
     *
     * @return int
     */
    public function getStreetsByMunicipalityCount(int $parentObjectId): int
    {
        return $this->getStreetsByMunicipality($parentObjectId)
            ->count();
    }

    /**
     * Returns streets by district count.
     *
     * @param int $parentObjectId
     *
     * @return int
     */
    public function getStreetsByDistrictCount(int $parentObjectId): int
    {
        return $this->getStreetsByDistrict($parentObjectId)
            ->count();
    }

}