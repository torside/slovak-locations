<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\MunicipalityCollection;
use Torside\SlovakLocations\Entities\MunicipalityEntity;

final class MunicipalityProvider
{

    const INDEX_NAME = 'municipalities';

    /** @var string $entity */
    private $entity = MunicipalityEntity::class;

    /** @var string $collection */
    private $collection = MunicipalityCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * MunicipalityProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for MunicipalityProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return MunicipalityEntity
     */
    public function getMunicipalityById(int $objectId): MunicipalityEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new MunicipalityEntity($data['_source']);
    }

    /**
     * Returns municipalities by county.
     *
     * @param int $parentObjectId
     *
     * @return MunicipalityCollection
     */
    public function getMunicipalitiesByCounty(int $parentObjectId): MunicipalityCollection
    {
        /** @var array $properties */
        $properties = ['objectId', 'versionId', 'municipalityName', 'municipalityCode', 'countyIdentifier', 'status', 'nominatimRef'];

        /** @var string $parentField */
        $parentField = 'countyIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => array_merge(
                $this->locationProvider->createSimpleFilter($parentField, $parentObjectId),
                ["_source" => $properties]
            )
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns municipalities by county count.
     *
     * @param int $parentObjectId
     *
     * @return int
     */
    public function getMunicipalitiesByCountyCount(int $parentObjectId): int
    {
        return $this->getMunicipalitiesByCounty($parentObjectId)
            ->count();
    }
}