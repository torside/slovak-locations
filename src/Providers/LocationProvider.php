<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use Torside\SlovakLocations\ElasticSearchConnect;
use Torside\SlovakLocations\Entities\GeoPointEntity;

final class LocationProvider
{

    const OPERATION_SEARCH = '_search';
    const OPERATION_DOC = 'doc';
    const RESULTS_LIMIT = 10000;

    /** @var ClientInterface $client */
    protected $client;

    /** @var int $indexVersion */
    protected $indexVersion;

    /**
     * LocationProvider constructor.
     *
     * @param ElasticSearchConnect $client
     * @param int $indexVersion
     */
    public function __construct(ElasticSearchConnect $client, int $indexVersion)
    {
        $this->client = $client;
        $this->indexVersion = $indexVersion;
    }

    /**
     * Sets current index version.
     *
     * @return int
     */
    public function getIndexVersion(): int
    {
        return $this->indexVersion;
    }

    /**
     * Returns current index version.
     *
     * @param int $indexVersion
     *
     * @return $this
     */
    public function setIndexVersion(int $indexVersion)
    {
        $this->indexVersion = $indexVersion;
        return $this;
    }

    /**
     * Returns ES http client connector.
     *
     * @return ClientInterface
     */
    public function getClient(): ClientInterface
    {
        return $this->client;
    }

    /**
     * Creates simple bool filter.
     *
     * @param string $field
     * @param mixed $value
     *
     * @return array
     */
    public function createSimpleFilter(string $field, $value): array
    {
        return [
            'size' => self::RESULTS_LIMIT,
            'query' => [
                'bool' => [
                    'filter' => [
                        [
                            'term' => [
                                $field => $value
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * Creates simple geo distance filter.
     *
     * @param GeoPointEntity $coordinates
     * @param int $size
     * @param string $distance
     * @param string $geoPointField
     *
     * @return array
     */
    public function createGeoDistanceFilter(GeoPointEntity $coordinates, int $size = 1, string $distance = '40km', string $geoPointField = 'coordinates'): array
    {
        return [
            "from" => 0,
            "size" => $size,
            "query" => [
                "bool" => [
                    "filter" => [
                        "geo_distance" => [
                            "distance" => $distance,
                            "$geoPointField" => [
                                "lat" => $coordinates->getLat(),
                                "lon" => $coordinates->getLon()
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }

}