<?php

namespace Torside\SlovakLocations\Providers;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use Torside\SlovakLocations\Collections\CountyCollection;
use Torside\SlovakLocations\Entities\CountyEntity;

final class CountyProvider
{

    const INDEX_NAME = 'counties';

    /** @var string $entity */
    private $entity = CountyEntity::class;

    /** @var string $collection */
    private $collection = CountyCollection::class;

    /** @var LocationProvider $locationProvider */
    private $locationProvider;

    /** @var ClientInterface $client */
    private $client;

    /** @var string $searchUri */
    private $searchUri;

    /** @var string $docUri */
    private $docUri;

    /**
     * CountyProvider constructor.
     *
     * @param LocationProvider $locationProvider
     */
    public function __construct(LocationProvider $locationProvider)
    {
        $this->locationProvider = $locationProvider;
        $this->init();
    }

    /**
     * Initializes ES http client connector and endpoint URIs for CountyProvider.
     */
    public function init()
    {
        $this->client = $this->locationProvider->getClient();
        $this->searchUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_SEARCH);
        $this->docUri = sprintf('/%s_v%d/%s', static::INDEX_NAME, $this->locationProvider->getIndexVersion(), LocationProvider::OPERATION_DOC);
    }

    /**
     * @param int $objectId
     *
     * @return CountyEntity
     */
    public function getCountyById(int $objectId): CountyEntity
    {
        /** @var string $uri */
        $uri = sprintf("%s/%d", $this->docUri, $objectId);

        /** @var Response $response */
        $response = $this->client->get($uri);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return new CountyEntity($data['_source']);
    }

    /**
     * Returns counties by region.
     *
     * @param int $parentObjectId
     *
     * @return CountyCollection
     */
    public function getCountiesByRegion(int $parentObjectId): CountyCollection
    {
        /** @var array $properties */
        $properties = ['objectId', 'versionId', 'countyName', 'countyCode', 'regionIdentifier'];

        /** @var string $parentField */
        $parentField = 'regionIdentifier';

        /** @var Response $response */
        $response = $this->client->post($this->searchUri, [
            RequestOptions::JSON => array_merge(
                $this->locationProvider->createSimpleFilter($parentField, $parentObjectId),
                ["_source" => $properties]
            )
        ]);

        /** @var array $data */
        $data = json_decode($response->getBody(), true);

        return $this->collection::make(array_map(function ($location) {
            return new $this->entity($location['_source']);
        }, $data['hits']['hits']));
    }

    /**
     * Returns counties by region count.
     *
     * @param int $parentObjectId
     *
     * @return int
     */
    public function getCountiesByRegionCount(int $parentObjectId): int
    {
        return $this->getCountiesByRegion($parentObjectId)
            ->count();
    }

}